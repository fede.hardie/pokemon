const POKEMON_LIMIT_PER_PAGE = 20;

const getPokemons = async (page = 3) => {
  const response = await fetch(`https://pokeapi.co/api/v2/pokemon?limit=${POKEMON_LIMIT_PER_PAGE}&offset=${POKEMON_LIMIT_PER_PAGE*page}`);
  if(response.ok){
      const {results} = await response.json();
      const arrayPokemon = results.map(pokemon => getPokemonDetails(pokemon.url));
      const allPokemonsResult = await Promise.all(arrayPokemon);
      const data = allPokemonsResult.map(({ weight, name, types:typesData, sprites })=>{
        const img = sprites.other['official-artwork'].front_default;
        const types = typesData.map(type => type.type.name);
          return { weight, name, types, img};
      });
      return data;
  }
  return null;
};

const getPokemonDetails = async (url) => {
    const response = await fetch(url);
    if(response.ok){
        return response.json();
    }
    return null;
}

const getPokemonByName = async (pokemonName) => {
    const response = await fetch(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`);
    if(response.ok){
        const results = await response.json();
        return results;
    }
    return null;
};


export { 
    getPokemons,
    getPokemonByName,
};
