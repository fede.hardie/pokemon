import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import PokemonCards from '../PokemonCards';
import PokemonDetail from "../PokemonDetail";

const AppRouter = () => {
    return (
        <Router>
            <Switch>
                <Route exact path="/" component={PokemonCards} />
                <Route exact path="/detail/:pokemonName" component={PokemonDetail} />
            </Switch>
        </Router>
    );
};

export default AppRouter;