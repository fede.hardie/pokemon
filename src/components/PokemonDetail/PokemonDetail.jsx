import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import { Link, useParams } from 'react-router-dom';
import { getPokemonByName } from "../../services/pokeApi";
import './styles.css';

const PokemonDetail = () =>{
    const { pokemonName } = useParams();
    const [ pokemon, setPokemon] = useState({});

    useEffect(() => {
        const init = async () =>{
          const data = await getPokemonByName(pokemonName);
          setPokemon(data);
        }
        init();
      }, [pokemonName]);
      
      console.log(pokemon);
        
    return (
        <div className="container">
            <br />
            <Link to="/">
                <FontAwesomeIcon className="icon-arrow" icon={faArrowLeft} title="Volver al listado" />
            </Link>
            <div className="text-center">
                <img src={pokemon?.sprites?.other['official-artwork']?.front_default} alt={pokemon?.name}></img>
            </div>
            <h3 className="text-uppercase">{pokemon?.name}</h3>
            <h5>Peso: {pokemon.weight}</h5>            
        </div>
    );
}

export default PokemonDetail;