import { faEye } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { Link } from 'react-router-dom';
import './styles.css';

const PokemonCard = ({pokemon}) => {
    return (
        <div className="col-xs-12 col-md-6 col-lg-4 p-4" >
               <div className="card">
                <img className="card-img-top" src={pokemon.img} alt={pokemon.name}></img>
                <div className="card-body">
                    <h5 className="card-title text-center">{pokemon.name}</h5>
                    <p className="card-text">Peso: {pokemon.weight}</p>
                    <p className="card-text">Tipo: {pokemon.types}</p>
                    <Link to={`/detail/${pokemon.name}`}>
                        <FontAwesomeIcon className="icon-detail" icon={faEye} title="Ver detalle" />
                    </Link>
                </div>
            </div>
        </div>
    );
}

export default PokemonCard;