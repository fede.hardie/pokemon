import React, { useEffect, useState } from 'react';
import { getPokemons } from '../../services/pokeApi';
import PokemonCard from '../PokemonCard';

const PokemonCards = () =>{

    const [ pokemons, setPokemons] = useState([]);
    useEffect(() => {
        const init = async () =>{
          const data = await getPokemons();
          setPokemons(data);
        }
        init();
      }, []);

      const cards = pokemons.map( (pokemon) => 
            <PokemonCard pokemon={pokemon} key={pokemon.name} />
      );
      
    return <div className="container">
        <div className="row">
            {cards}
        </div>
    </div>;
}

export default PokemonCards;