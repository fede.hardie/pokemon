import React from 'react';
import AppRouter from '../router/AppRouter';
import 'bootstrap/dist/css/bootstrap.min.css';

const MainApp = () => {
  return <AppRouter />;
};

export default MainApp;
